import * as React from "react";

import VendingMachineContainer from "./components/vending-machine.container";
import "./index.scss";

class App extends React.Component {
  public render() {
    return (
      <div className="vending-machine-container">
        <VendingMachineContainer />
      </div>
    );
  }
}

export default App;
