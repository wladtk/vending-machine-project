import React, { FunctionComponent } from "react";

import "./arm.scss";

const Arm: FunctionComponent<object> = () => (
  <div className="arm">
    <div className="hand" />
  </div>
);

export default Arm;
