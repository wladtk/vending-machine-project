import React, { FunctionComponent } from "react";
import PropTypes from "prop-types";

import { ICanProps } from "../../interfaces/props";

import "./can.scss";

const Can: FunctionComponent<ICanProps> = ({ row, column, taste }) => (
  <div className="container" data-can={column}>
    <div className="can">
      <span className="taste">{taste}</span>
    </div>
    <div className="can-label">{`${row}${column}`}</div>
  </div>
);

Can.propTypes = {
  row: PropTypes.string.isRequired,
  column: PropTypes.string.isRequired,
  taste: PropTypes.string.isRequired
};

export default Can;
