import React, { FunctionComponent } from "react";
import PropTypes from "prop-types";

import { IButtonProps } from "../../../interfaces/props";

import "./button.scss";

const Button: FunctionComponent<IButtonProps> = ({
  value,
  disabled,
  children,
  type,
  ...props
}) => {
  return (
    <button value={value} disabled={disabled} {...props}>
      {children}
    </button>
  );
};

Button.propTypes = {
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  disabled: PropTypes.bool.isRequired,
  type: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired,
  onClick: PropTypes.func
};

Button.defaultProps = {
  value: "",
  disabled: false,
  className: "button",
  type: "button",
  onClick: () => null
};

export default Button;
