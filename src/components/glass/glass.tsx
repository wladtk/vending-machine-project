import React, { FunctionComponent } from "react";

import "./glass.scss";

const Glass: FunctionComponent = () => <div className="glass" />;

export default Glass;
