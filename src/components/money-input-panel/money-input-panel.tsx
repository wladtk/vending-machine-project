import React, {
  useState,
  useRef,
  useCallback,
  FunctionComponent,
  useEffect
} from "react";
import PropTypes from "prop-types";

import Button from "../common/button/button";
import { IMoneyInputProps } from "../../interfaces/props";

import "./money-input-panel.scss";

const MoneyInputPanel: FunctionComponent<IMoneyInputProps> = ({
  onBalanceChange,
  currentBalance,
  priceToPay,
  onReadyToPay
}) => {
  const inputRef = useRef<HTMLInputElement>(null);
  const [inputtedMoney, setInputtedMoney] = useState(0);

  useEffect(() => {
    if (currentBalance > 0 && priceToPay > 0 && currentBalance >= priceToPay) {
      onReadyToPay(true);
    }
    setInputtedMoney(+currentBalance);
  }, [currentBalance]);

  const processMoneyInput = useCallback(
    (event: React.FormEvent<HTMLFormElement>) => {
      event.preventDefault();

      if (inputRef && inputRef.current) {
        setInputtedMoney(+inputRef.current.value + inputtedMoney);
        onBalanceChange(+inputRef.current.value + inputtedMoney);
        inputRef.current.value = "";
      }
    },
    [inputtedMoney]
  );

  return (
    <div className="money-input-panel">
      <h3 className="title">Money Deposited</h3>
      <form onSubmit={processMoneyInput}>
        <input
          className="input"
          ref={inputRef}
          type="number"
          step="0.25"
          min="0"
          max="10"
          placeholder="Enter money"
        />
        <Button type="submit" value="Confirm" className="button simple-button">
          Confirm
        </Button>
      </form>
      {!!currentBalance && (
        <span className="entered-money-caption">
          You entered {currentBalance}$
        </span>
      )}
    </div>
  );
};

MoneyInputPanel.propTypes = {
  onBalanceChange: PropTypes.func.isRequired,
  onReadyToPay: PropTypes.func.isRequired,
  priceToPay: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    .isRequired,
  currentBalance: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    .isRequired
};

export default MoneyInputPanel;
