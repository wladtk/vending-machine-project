import React, { useCallback, FunctionComponent, useEffect } from "react";

import { ISelectDrinkPanelProps } from "../../interfaces/props";
import Button from "../common/button/button";

import "./select-drink-panel.scss";

const SelectDrinkPanel: FunctionComponent<ISelectDrinkPanelProps> = ({
  drinks,
  activeRow,
  activeColumn,
  onSelect,
  onReset,
  onSetRow,
  onSetColumn
}) => {
  const rowsMarkers = ["A", "B", "C", "D", "E"];
  const columnsMarkers = ["1", "2", "3", "4", "5"];

  useEffect(() => {
    let selectedDrink = {};
    if (!!activeRow && !!activeColumn) {
      drinks.map(data => {
        data.drinks.map(drink => {
          if (
            String(drink.x) === activeRow &&
            String(drink.y) === activeColumn
          ) {
            selectedDrink = drink;
            onSelect(selectedDrink);
          }
        });
      });
    }
  }, [activeRow, activeColumn]);

  return (
    <div className="panel">
      <h3 className="title">Select Drink</h3>
      <span className="input">{`${activeRow}${activeColumn}`}</span>
      <div className="letters row">
        {rowsMarkers.map(rowLetter => (
          <Button
            key={rowLetter}
            className="button"
            value={rowLetter}
            disabled={!!activeRow}
            onClick={useCallback((e: any) => onSetRow(e.target.value), [
              onSetRow
            ])}
          >
            {rowLetter}
          </Button>
        ))}
      </div>
      <div className="digits row">
        {columnsMarkers.map(digit => (
          <Button
            key={digit}
            className="button"
            value={digit}
            disabled={!!activeColumn || !activeRow}
            onClick={useCallback((e: any) => onSetColumn(e.target.value), [
              onSetColumn
            ])}
          >
            {digit}
          </Button>
        ))}
      </div>
      {activeRow && activeColumn && (
        <Button
          className="button simple-button"
          value="reset"
          onClick={onReset}
        >
          Reset selection
        </Button>
      )}
    </div>
  );
};

export default SelectDrinkPanel;
