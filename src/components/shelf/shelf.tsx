import React, { FunctionComponent } from "react";
import PropTypes from "prop-types";

import { IShelfProps } from "../../interfaces/props";

import "./shelf.scss";

const Shelf: FunctionComponent<IShelfProps> = ({ letter, children }) => (
  <div className="shelf" data-shelf={letter}>
    {children}
  </div>
);

Shelf.propTypes = {
  letter: PropTypes.oneOf(["A", "B", "C", "D", "E"]).isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired
};

export default Shelf;
