import React, { FunctionComponent } from "react";
import PropTypes from "prop-types";

import { ITrayProps } from "../../interfaces/props";

import "./tray.scss";

const Tray: FunctionComponent<ITrayProps> = ({ drink, isPaymentOk }) => (
  <>
    <div className={`tray-inner ${isPaymentOk ? "open" : ""}`}>
      <div className="container" data-can={drink.column}>
        <div className="can" data-taste={drink.taste} />
      </div>
    </div>
    <div className={`tray ${isPaymentOk ? "open" : ""}`} />
  </>
);

Tray.propTypes = {
  drink: PropTypes.shape({
    column: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
      .isRequired,
    taste: PropTypes.string.isRequired
  }).isRequired,
  isPaymentOk: PropTypes.bool.isRequired
};

Tray.defaultProps = {
  isPaymentOk: false
};

export default Tray;
