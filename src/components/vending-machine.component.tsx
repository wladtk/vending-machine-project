import React, { useState, useEffect, FunctionComponent } from "react";
import _ from "lodash";

import SelectDrinkPanel from "./select-drink-panel/select-drink-panel";
import MoneyInputPanel from "./money-input-panel/money-input-panel";
import Glass from "./glass/glass";
import Shelf from "./shelf/shelf";
import Tray from "./tray/tray";
import Arm from "./arm/arm";
import Can from "./can/can";
import { IVendingMachineProps } from "../interfaces/props";

import "./vending-machine.component.scss";

export const VendingMachine: FunctionComponent<IVendingMachineProps> = ({
  fetchDrinks,
  setBalance,
  setSelectedDrink,
  setReadyForPurchaseStatus,
  resetVendingMachine,
  drinks,
  currentBalance,
  selectedDrink,
  isPaymentOk
}) => {
  const [activeRow, setActiveRow] = useState("");
  const [activeColumn, setActiveColumn] = useState("");

  useEffect(() => {
    fetchDrinks();
  }, []);

  const handleBalanceChange = (balance: number) => {
    setBalance(balance);
  };

  const handleMachineReset = () => {
    setActiveRow("");
    setActiveColumn("");
    resetVendingMachine();
  };

  const handleSetRow = (row: string) => {
    setActiveRow(row);
  };

  const handleSetColumn = (column: string) => {
    setActiveColumn(column);
  };

  const completeDrinkPurchase = (isReady: boolean) => {
    setReadyForPurchaseStatus(isReady);

    setTimeout(() => {
      handleMachineReset();
    }, 3000);
  };

  const composedData = _.chain(drinks)
    .groupBy("x")
    .map((drink, key) => ({ row: key, drinks: drink }))
    .value();

  return (
    <div className="machine-wrap">
      <div className="machine" data-price={selectedDrink.price}>
        <div className="inner">
          <Arm />

          {composedData.map(data => (
            <Shelf key={data.row} letter={data.row}>
              {data.drinks.map(drink => (
                <Can
                  key={`${data.row}${drink.y}`}
                  row={data.row}
                  column={`${drink.y}`}
                  taste={drink.taste}
                />
              ))}
            </Shelf>
          ))}
        </div>

        <Glass />

        <Tray
          drink={{ column: selectedDrink.y, taste: selectedDrink.taste }}
          isPaymentOk={isPaymentOk}
        />
      </div>

      <div className="numpad">
        <SelectDrinkPanel
          drinks={composedData}
          onSelect={setSelectedDrink}
          activeRow={activeRow}
          activeColumn={activeColumn}
          onSetRow={handleSetRow}
          onSetColumn={handleSetColumn}
          onReset={handleMachineReset}
        />

        <div
          className={`${
            !selectedDrink.y && !selectedDrink.price ? "disabled" : ""
          }`}
        >
          <MoneyInputPanel
            onBalanceChange={handleBalanceChange}
            currentBalance={currentBalance}
            priceToPay={selectedDrink.price}
            onReadyToPay={completeDrinkPurchase}
          />
          {isPaymentOk && (
            <span className="entered-money-caption">
              Receive your change:
              {`${+currentBalance - +selectedDrink.price}$`}
            </span>
          )}
        </div>
      </div>
    </div>
  );
};

export default VendingMachine;
