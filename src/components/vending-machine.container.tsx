import { connect } from "react-redux";
import { IApplicationState } from "../interfaces";

import { Dispatch } from "redux";

import { requestDrinks } from "../redux/actions/drinks";
import {
  setBalance,
  setSelectedDrink,
  setReadyForPurchaseStatus,
  resetVendingMachine
} from "../redux/actions/settings";

import VendingMachine from "./vending-machine.component";

const mapStateToProps = (state: IApplicationState) => ({
  drinks: state.drinks.list,
  currentBalance: state.settings.currentBalance,
  selectedDrink: state.settings.selectedDrink,
  isPaymentOk: state.settings.isPaymentOk
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
  fetchDrinks: () => dispatch(requestDrinks()),
  setBalance: (balance: number) => dispatch(setBalance(balance)),
  setSelectedDrink: (drink: any) => dispatch(setSelectedDrink(drink)),
  setReadyForPurchaseStatus: (isPaymentOk: boolean) =>
    dispatch(setReadyForPurchaseStatus(isPaymentOk)),
  resetVendingMachine: () => dispatch(resetVendingMachine())
});

export default connect(mapStateToProps, mapDispatchToProps)(VendingMachine);
