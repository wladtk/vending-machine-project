export interface Drink {
  price: number,
  taste: string,
  amount: number,
  x: string,
  y: string
}
  
  export interface DrinksState {
    readonly list: Drink[],
    readonly isLoading: boolean,
    readonly error: string
  }