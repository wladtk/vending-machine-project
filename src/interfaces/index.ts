import { Drink } from './drinks';

export interface Action {
  type: string,
  payload?: object
}

export interface IApplicationState {
  readonly drinks: {
    list: Drink[]
  }
  readonly settings: {
    currentBalance: number;
    selectedDrink: any;
    isPaymentOk: boolean;
  },
}