import { Drink } from './drinks';

export interface IButtonProps {
  value: number | string;
  disabled?: boolean;
  children: any;
  onClick?: any;
  type?: string;
  className?: string;
}

export interface ICanProps {
  row: string;
  column: string;
  taste: string;
}

export interface IShelfProps {
  letter: string;
  children: JSX.Element | React.ReactNode;
}

export interface ITrayProps {
  drink: {
    column: string | number;
    taste: string;
  },
  isPaymentOk: boolean;
}

export interface ISelectDrinkPanelProps {
  drinks: Array<{
    row: string;
    drinks: Drink[]
  }>;
  onSelect: (drink: any) => void;
  onReset: () => void;
  onSetRow: (row: string) => void; 
  onSetColumn: (column: string) => void; 
  activeRow: string;
  activeColumn: string;
}

export interface IVendingMachineProps {
  fetchDrinks: () => void,
  setBalance: (balance: number) => void,
  setSelectedDrink: (drink: any) => void,
  setReadyForPurchaseStatus: (isPaymentOk: boolean) => void,
  resetVendingMachine: () => void;
  drinks: Drink[],
  currentBalance: string | number,
  selectedDrink: any,
  isPaymentOk: boolean
}

export interface IMoneyInputProps {
  onBalanceChange: (balance: number) => void;
  onReadyToPay: (isReady: boolean) => void;
  currentBalance: string | number;
  priceToPay: string | number;
}