export interface Banknotes {
    quarter: number,
    halfDollar: number,
    dollar: number,
}

export interface SettingsState {
    currentBalance: number,
    selectedDrink: any,
    isPaymentOk: boolean
}