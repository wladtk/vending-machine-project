export enum DrinksActionTypes {
  FETCH_REQUEST = '@@drinks/FETCH_REQUEST',
  FETCH_SUCCESS = '@@drinks/FETCH_SUCCESS',
  FETCH_ERROR = '@@drinks/FETCH_ERROR',
  SELECTED = '@@drinks/SELECTED'
}