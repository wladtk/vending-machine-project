export enum SettingsActionTypes {
  SET_BALANCE = '@@settings/SET_BALANCE',
  SET_SELECTED_DRINK = '@@settings/SET_SELECTED_DRINK',
  SET_IS_PAYMENT_OK_STATUS = '@@settings/SET_IS_PAYMENT_OK_STATUS',
  RESET_VENDING_MACHINE = '@@settings/RESET_VENDING_MACHINE'
}