import { action } from 'typesafe-actions'

import { Drink } from '../../interfaces/drinks'
import { DrinksActionTypes } from '../action-types/drinks';

export const requestDrinks = () => action(DrinksActionTypes.FETCH_REQUEST);
export const requestDrinksSuccess = (data: Drink[]) => action(DrinksActionTypes.FETCH_SUCCESS, data);
export const requestDrinksError = (error: object | string) => action(DrinksActionTypes.FETCH_ERROR, error);