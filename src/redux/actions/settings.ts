import { action } from 'typesafe-actions'

import { SettingsActionTypes } from '../action-types/settings';

export const setBalance = (balance: number) => action(SettingsActionTypes.SET_BALANCE, balance);
export const setSelectedDrink = (drink: any) => action(SettingsActionTypes.SET_SELECTED_DRINK, drink);
export const setReadyForPurchaseStatus = (isPaymentOk: boolean) => action(SettingsActionTypes.SET_IS_PAYMENT_OK_STATUS 
, isPaymentOk);
export const resetVendingMachine = () => action(SettingsActionTypes.RESET_VENDING_MACHINE)