import { Action } from '../../interfaces/';
import { DrinksState } from '../../interfaces/drinks';
import { DrinksActionTypes } from '../action-types/drinks';

const initialState: DrinksState = {
    list: [],
    isLoading: false,
    error: ''
}

export const drinksReducer = (state = initialState, action: Action) => {
    const { type } = action;
    switch (type) {
        case DrinksActionTypes.FETCH_REQUEST: {
            return { ...state, isLoading: true }
        }
        case DrinksActionTypes.FETCH_SUCCESS: {
            return { ...state, isLoading: false, list: action.payload }
        }
        case DrinksActionTypes.FETCH_ERROR: {
            return { ...state, isLoading: false, error: action.payload }
        }
        default:
            return state;
    }
}