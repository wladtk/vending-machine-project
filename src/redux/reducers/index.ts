import { combineReducers } from 'redux';

import { drinksReducer } from './drinks'
import { settingsReducer } from './settings';

export default combineReducers({
  drinks: drinksReducer,
  settings: settingsReducer
})