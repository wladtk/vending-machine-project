import { Action } from '../../interfaces/';
import { SettingsState } from '../../interfaces/settings';
import { SettingsActionTypes } from '../action-types/settings';

const initialState: SettingsState = {
    currentBalance: 0,
    selectedDrink: {
        price: 0,
        taste: '',
        amount: 0,
        x: '',
        y: ''
    },
    isPaymentOk: false
}

export const settingsReducer = (state = initialState, action: Action) => {
    const { type } = action;
    switch (type) {
        case SettingsActionTypes.SET_BALANCE: 
            return {
                ...state,
                currentBalance: action.payload
            }
        case SettingsActionTypes.SET_SELECTED_DRINK: 
            return {
                ...state,
                selectedDrink: action.payload
            }
        case SettingsActionTypes.SET_IS_PAYMENT_OK_STATUS:
            return {
                ...state,
                isPaymentOk: action.payload
            }
            case SettingsActionTypes.RESET_VENDING_MACHINE:
                return initialState;
        default:
            return state;
    }
}