import { all, call, fork, put, takeEvery } from "redux-saga/effects";

import { DrinksActionTypes } from "../action-types/drinks";
import { requestDrinksSuccess, requestDrinksError } from "../actions/drinks";
import config from "../../config";

function* handleDrinksRequest() {
  try {
    const response = yield call(() =>
      fetch(`${config.apiUrl}get_all_product`, {
        method: "GET"
      }).then((res: any) => res.json())
    );

    if (response.error) {
      yield put(requestDrinksError(response.error));
    } else {
      yield put(requestDrinksSuccess(response.data));
    }
  } catch (err) {
    if (err instanceof Error) {
      yield put(requestDrinksError(err));
    } else {
      yield put(requestDrinksError("An unknown error has occurred"));
    }
  }
}

// This is our watcher function. We use `take*()` functions to watch Redux for a specific action
// type, and run our saga, for example the `handleFetch()` saga above.
function* watchFetchRequest() {
  yield takeEvery(DrinksActionTypes.FETCH_REQUEST, handleDrinksRequest);
}

// Export our root saga.
// We can also use `fork()` here to split our saga into multiple watchers.
export function* drinksSaga() {
  yield all([fork(watchFetchRequest)]);
}
