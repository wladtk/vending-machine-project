import { all, fork } from "redux-saga/effects";
import { drinksSaga } from "./drinks";

// We `fork()` these tasks so they execute in the background.
export function* rootSaga() {
  yield all([fork(drinksSaga)]);
}
